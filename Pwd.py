#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Password generator module.

requires Ui_PwdGen

"""

from random import *
from Ui_PwdGen import *

def RndPass(iLen, bNoSpecChar, bKvaziWord, bLowCase):
    """
    RndPass (iLen - длина пароля,
                bNoSpecChar - без спецсимволов,
                bKvaziWord - легко запоминаемый,
                bLowCase - без больших букв)
    """
    
    # RndPass(8;1;1)
    # длина = 8, со спец-символами, похоже на слово, маленикими буквами
    C1 = '#$%&+-'
    l1 = 6
    C2 = 'aeiouy'
    l2 = 6
    C3 = 'AEUY'
    l3 = 4
    C4 = 'bcdfghjklmnpqrstvwxz'
    l4 = 20
    C5 = 'BCDFGHJKLMNPQRSTVWXZ'
    l5 = 20
    C6 = '123456789'
    l6 = 9
    # засев ГСЧ
    seed()
    # длина не менее 8
    if iLen < 8:
        Length = 8
    else:
        Length = iLen
    # первый символ буква или цифра
    x = randrange(5)+2
    if (x == 2):
        p = C2[randrange(l2)]
        vow = True
    elif (x == 3):
        p = C3[randrange(l3)]
        vow = True
    elif (x == 4):
        p = C4[randrange(l4)]
        vow = False
    elif (x == 5):
        p = C5[randrange(l5)]
        vow = False
    elif (x == 6):
        p = C6[randrange(l6)]
        vow = True
    # дальше случайно до какого-то символа заполнить буквами
    # причем можно квазисловом
    l = len(p) + 1 + randrange(int(Length / 1.7))
    for i in range(len(p) + 1, l):
        x = 1 + randrange(2)
        if bKvaziWord:
            if vow:
                x = x + 2
            vow = not vow
        else:
            x = x + 1 + randrange(2)
        if (x == 1):
            p = p + C2[randrange(l2)]
        elif (x == 2):
            p = p + C3[randrange(l3)]
        elif (x == 3):
            p = p + C4[randrange(l4)]
        elif (x == 4):
            p = p + C5[randrange(l5)]
    # спецсимвол или цифра 1
    x = randrange(1, 2)
    if not bNoSpecChar:
        if (x == 1):
            p = p + C1[randrange(l1)]
        elif (x == 2):
            p = p + C6[randrange(l6)]
    else:
        p = p + C6[randrange(l6)]
    # начало аналогичной 2й части
    vow = bool(randrange(2))
    # с длиной - до предпоследнего символа
    l = len(p) + 1 + randrange(int(Length / 1.7))
    for i in range(len(p) + 1, l):
        x = 1 + randrange(2)
        if bKvaziWord:
            if vow:
                x = x + 2
            vow = not vow
        else:
            x = x + 1 + randrange(2)
        if (x == 1):
            p = p + C2[randrange(l2)]
        elif (x == 2):
            p = p + C3[randrange(l3)]
        elif (x == 3):
            p = p + C4[randrange(l4)]
        elif (x == 4):
            p = p + C5[randrange(l5)]
    # спецсимвол или цифра 2
    if not bNoSpecChar:
        if (x == 1):
            p = p + C1[randrange(l1)]
        elif (x == 2):
            p = p + C6[randrange(l6)]
    else:
        p = p + C6[randrange(l6)]
    # начало аналогичной 3й части
    vow = bool(randrange(2))
    # с длиной - до последнего символа
    for i in range(len(p) + 1, Length+1):
        x = 1 + randrange(2)
        if bKvaziWord:
            if vow:
                x = x + 2
            vow = not vow
        else:
            x = x + 1 + randrange(2)
        if (x == 1):
            p = p + C2[randrange(l2)]
        elif (x == 2):
            p = p + C3[randrange(l3)]
        elif (x == 3):
            p = p + C4[randrange(l4)]
        elif (x == 4):
            p = p + C5[randrange(l5)]
    if bLowCase:
        p = p.lower()
    return p

def PwdSimple(iLength):
    """
    PwdSimple(iLength - длина пароля)
    выдает простой пароль - строку случайных символов
    """
    
    #48-57 = 0 To 9, 65-90 = A To Z, 97-122 = a To z
    #amend For other characters If required
    strTemp = ""
    for i in range(1, iLength):
        while 1:
            iTemp = 48 + 1 + randrange(122)
            if (48 <= iTemp <= 57) or (65 <= iTemp <= 90) or (97 <= iTemp <= 122):
                bOK = True
            else:
                bOK = False
            if bOK == True:
                break
        bOK = False
        strTemp = strTemp + chr(iTemp)
    return strTemp

def gen_pwd():
    """
    button clicked signal interpreter
    """
    
    pLen=ui.spinPwdLen.value()
    pNSC=ui.fNoSpChar.isChecked()
    pQW=ui.fQuasiWord.isChecked()
    pLC=ui.fLowCase.isChecked()
    ui.linePwd.setText(RndPass(pLen, pNSC, pQW, pLC))
    app.processEvents()
    
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    PwdGenMW = QtWidgets.QMainWindow()
    ui = Ui_PwdGenMW()
    ui.setupUi(PwdGenMW)
    # connect button to function
    ui.btnGen.clicked.connect(gen_pwd)
    PwdGenMW.show()
    sys.exit(app.exec_())
